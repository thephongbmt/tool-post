
const message = require('./message');


const ROLE_CUSTOMER = 'Customer';
const ROLE_ADMIN = 'Admin';

const STATUS_INACTIVE = 'inactive';
const STATUS_ACTIVE = 'active';
const STATUS_DELETE = 'delete';

const FACEBOOK = 'facebook';
const YOUTUBE = 'youtube';

const USER = 'user';
const GROUP = 'group';
const FANPAGE = 'fanpage';

const DATE_FORMAT_SCHEDULE =  'YYYY-MM-DD HH:mm';
const SCHEDULE_POST = 'schedule post';
const POST = 'post';
const ACTIVE_CRON_POST = false;
const TIME_RUN_CRON = '0 */12 * * *';
const ONE_HOURS_RUN_CRON = '0 */1 * * *';
const SIX_HOURS_RUN_CRON = '0 */6 * * *';
const ONE_DAY_RUN_CRON = '0 0 */1 * *';
const TWO_DAY_RUN_CRON = '0 0 */2 * *';
const LIMIT_POST = 999;


module.exports = {
  ERROR_TYPE: {
    CALLING_FACEBOOK_FAIL: 'CallFacebookFail',
    CALLING_YOUTUBE_FAIL : 'CallYoutubeFail',
    BAD_REQUEST          : 'BadRequest',
    INTERNAL_SERVER      : 'InternaServer'
  },
  DEFAULT_PUBLIC_ERROR: false,
  FORMAT_DATE_DEFAULT : 'DD-MM-YYYY HH:mm:ss',
  //ROLE
  ACCOUNT_ROLE        : {
    ENUM    : [ROLE_CUSTOMER, ROLE_ADMIN],
    ADMIN   : ROLE_ADMIN,
    CUSTOMER: ROLE_CUSTOMER
  },
  SOCIAL: {
    ENUM: [ FACEBOOK, YOUTUBE ],
    FACEBOOK, YOUTUBE
  },
  POST_TO_OBJECT: {
    ENUM: [ GROUP, USER, FANPAGE ],
    GROUP, FANPAGE, USER
  },
  TYPE_FANPAGE: {
    ENUM: [ GROUP, FANPAGE ],
    GROUP, FANPAGE
  },
  //get constaint in message
  //constain STATUS
  MESSAGE          : message,
  DB_RECONNECT_TIME: 1000,
  STATUS           : {
    ACTIVE  : STATUS_ACTIVE,
    INACTIVE: STATUS_INACTIVE,
    DELETE  : STATUS_DELETE,
    ENUM    : [STATUS_ACTIVE, STATUS_INACTIVE, STATUS_DELETE],
    DEFAULT : STATUS_ACTIVE
  },
  TYPE_WORKPLACE: {
    FACEBOOK: FACEBOOK,
    YOUTUBE : YOUTUBE,
    ENUM    : [FACEBOOK, YOUTUBE]
  },
  DATE_FORMAT_SCHEDULE: DATE_FORMAT_SCHEDULE,
  TYPE_POST           : {
    SCHEDULE_POST: SCHEDULE_POST,
    POST         : POST,
    ENUM         : [SCHEDULE_POST, POST]
  },
  CRON_POST: {
    ACTIVE_CRON_POST: ACTIVE_CRON_POST,
    TIME_RUN_CRON   : TIME_RUN_CRON,
    LIMIT_POST      : LIMIT_POST,
  },
  CRON_SETTING: {
    '1H'                 : ONE_HOURS_RUN_CRON,
    '6H'                 : SIX_HOURS_RUN_CRON,
    '1D'                 : ONE_DAY_RUN_CRON,
    '2D'                 : TWO_DAY_RUN_CRON,
    ALL_SETTING_CRON_TIME: ['1H', '6H', '1D', '2D']
  },
};

