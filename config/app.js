require('dotenv').config();
module.exports = {
  DEFAUL_LIMIT: 20,
  DOMAIN_FE   : process.env.EMAIL_SMTP_HOST || 'http://localhost:3000',
  FACEBOOK    : {
    URL    : 'https://graph.facebook.com/v4.0',
    VERSION: '4.0'
  },
  YOUTUBE: {
    URL    : 'https://www.googleapis.com/youtube/v3',
    VERSION: 'v3',
    KEY    : process.env.KEY_YOUTUBE || 'AIzaSyD15ONELcbkPIb_pG0ZBr6YUldkWxE94bM'
  },
  EMAIL_CONFIG: {
    HOST       : process.env.EMAIL_SMTP_HOST || 'smtp.gmail.com',
    PORT       : process.env.EMAIL_SMTP_PORT || 465,
    IGNORE_TLS : process.env.EMAIL_SMTP_IGNORE_TLS || true,
    SMTP_SECURE: true,
    USER       : process.env.EMAIL_SMTP_USER || 'noreply.by.random@gmail.com',
    PASS       : process.env.EMAIL_SMTP_PASS || 'Haha@1234'
  },
  //get constant in config
  PORT       : process.env.PORT,
  SENTRY     : process.env.SENTRY,
  ENV        : process.env.NAME,
  DESCRIPTION: process.env.DESCRIPTION,
  JWT        : {
    SECRET              : process.env.JWT_SECRET_KEY ||  'th1s1sm4p40n9',
    ALG                 : 'HS256',
    typ                 : 'JWT',
    EXPIRE_ACCESS_TOKEN : 7 * 24 * 60 * 60, //1h
    EXPIRE_REFRESH_TOKEN: 60 * 60 * 24 * 7 // 1 week
  },

  //Constaint for DB
  DB_HOST          : process.env.MONGO_HOST,
  DB_NAME          : process.env.MONGO_NAME,
  DB_PORT          : process.env.MONGO_PORT,
  DB_RECONNECT_TIME: 1000 // ms
};
