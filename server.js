const express = require('express');
const bodyParser = require('body-parser');
const Raven = require('raven');
const compression = require('compression');
const cors = require('cors');
const ECT = require('ect');
const router = require('./api');
const { middleware } = require('./middleware/express');
const { SENTRY, PORT, ENV, DESCRIPTION, DB_HOST, DB_PORT, DB_NAME } = require('./config/app');
const Log = require('./lib/Log.class');
const MongoDB  = require('./helpers/mongo');
const CronJob  = require('./helpers/cron');

//connect to database
const database = new MongoDB(DB_HOST, DB_PORT, DB_NAME);
database.connect();

const app = express();
//MIDDLE WARE
let ectRenderer = new ECT({
  watch: true,
  gzip : true,
  root : `${process.cwd()}/assets`,
  ext  : '.ect'
});
global.ectRenderer = ectRenderer;

//Handler body parser Request
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }, bodyParser.json()));
app.use(bodyParser.json());
//compress response
app.use(compression());
//cross origin resource sharing
app.use(cors());
//public resource
app.use('/static', express.static('assets/public'));

//RAVEN
// Must configure Raven before doing anything else with it
if (SENTRY) {
  Raven.config(SENTRY, { environment: ENV }).install();
  // The request handler must be the first middleware on the app
  app.use(Raven.requestHandler());
}

middleware.handleResponse(app);
//ROUTER
//check API test is live
app.get('/ping', (req, res) => {
  res.success('pong');
});
//init route
router(app);
// start cronJob
CronJob.startCronJob();
//check request not found
middleware.handleNotFoundRequest(app);
//MIDDLE WARE Handler Error
//handle error
middleware.handleError(app);
//Raven
app.use(Raven.errorHandler());

const server = app.listen(PORT || 3000, () => {
  let port = server.address().port;
  let host = server.address().address === '::' ? 'localhost' : server.address().address;
  Log.info(
    `- ${DESCRIPTION}
  + Server is running at http://${host}:${port}
  + API ENV: ${ENV || 'development'}`,
    'green'
  );
});

module.exports = server;
