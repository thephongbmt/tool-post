const workplaceModel = require('./model');
module.exports = {
  find: (query, option) =>{
    if(option){
      return workplaceModel.find(query).skip(option.skip).limit(option.limit);
    }
    return workplaceModel.find(query);
  },
  insertWorkplace: async (workplace) => {
    return workplaceModel.create(workplace);
  },
  findById: (id)=>{
    return workplaceModel.findById(id);
  },
  getWorkplaceByEmail: (email)=>{
    return workplaceModel.findOne({email: email});
  },
  findOne: (param)=>{
    return workplaceModel.findOne(param);
  },
  findByIdAndUpdate: (id, params)=>{
    return workplaceModel.findByIdAndUpdate(id, params);
  },
  findOneAndUpdate: (query, params)=>{
    return workplaceModel.findOneAndUpdate(query, params);
  },
  count: (query)=>{
    return workplaceModel.countDocuments(query);
  },
  findByIdAndRemove: (id) => {
    return workplaceModel.findByIdAndRemove(id);
  }
};
