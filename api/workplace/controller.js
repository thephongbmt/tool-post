const _ = require('lodash');
const workplaceService = require('./service');
const userService = require('../user/service');

const postService = require('../post/service');
const fanpageSerivce = require('../fanpage/service');

const youtubeService = require('../youtube/service');
const channelsService = require('../channels/service');

const APIError = require('../../lib/APIError');
const utilities = require('../../helpers/utilities');
const facebookHelper = require('../../helpers/facebook');
const { DEFAUL_LIMIT } = require('../../config/app');
const {  MESSAGE, STATUS, ERROR_TYPE }  = require('../../constant');
module.exports = {
  addFanpageIdToWorkplace: async(req, res)=>{
    try{
      let workplace = req.params.id;
      let workplaceDb = await workplaceService.findById(workplace);
      if(!workplaceDb){
        return res.error('Workplace is not found');
      }
      let fanpageIds = utilities.getValidArray(req.body.fanpageIds);

      if(!fanpageIds) {
        return res.error('Missing require field');
      }
      let user = await userService.findOne({ isTokenFBExpire: false, createdBy: req.user._id});
      //prepare user info when add to workplace
      let userInfos = {
        workplace: {
          _id: workplaceDb._id
        },
        user: {
          _id: user._id,
        },
        groups: []
      };
      for(let id of fanpageIds){
        try{
          if(id){
            let tmp = await facebookHelper.getFanpageInfo(id, {access_token: user.accessToken});
            userInfos.groups.push(tmp);
          }
        } catch (e){
          continue;
        }
      }
      // build userInfo
      if(!userInfos.groups.length === 0){
        return res.success('Fanpage is not exit', 400);
      }
      let allFanpage = await validationAndGetFormat(userInfos);
      return res.success(allFanpage, 'Add Success');
    }catch(e){
      return res.error(e, 500);
    }
  },
  createWorkplace: async(req, res) => {
    try {
      let body = req.body;
      let data = {
        name     : utilities.getValidString(body.name),
        createdBy: req.user._id,
        status   : utilities.getValidString(body.status) || STATUS.ACTIVE,
        type     : utilities.getValidString(body.type),
      };
      if(!data.name){
        return res.error('Missing require field', 400);
      }
      if(!data.type || !['youtube', 'facebook'].includes(data.type)){
        return res.error('type much be enum=[\'youtube\', \'facebook\']', 400);
      }
      if(STATUS.ENUM.indexOf(data.status) === -1){
        return res.error(`Status is not in ${STATUS.ENUM.toString()}`, 400);
      }
      let newWorkplace = await workplaceService.insertWorkplace(data);
      return res.success(newWorkplace, MESSAGE.GET_SUCCESS, 200);
    } catch (error) {
      res.error(error, 500);
    }
  },
  updateWorkplace: async (req, res) => {
    try {
      let body = req.body;
      let id = req.params.id;
      let params = {
        name  : body.name,
        status: body.status,
      };
      if(!params.status && !params.name){
        return res.error('Missing require field(name or status)', 400);
      }
      if(params.status && STATUS.ENUM.indexOf(params.status) === -1){
        return res.error(`Status is not in ${STATUS.ENUM.toString()}`, 400);
      }
      params = utilities.removeEmptyFields(params);
      let workplace = await workplaceService.findById(id);
      if(!workplace){
        return res.error(`Workplace is not found with id =${id}`, 400);
      }
      workplace = await workplaceService.findByIdAndUpdate(id, params);
      await workplace.save();
      return res.success(workplace, MESSAGE.GET_SUCCESS, 200);
    } catch (error){
      return res.error(error, 500);
    }
  },
  addFanpageToWorkPlace: async(req, res)=>{
    try {
      let body = req.body;
      let userInfos = utilities.getValidArray(body.userInfos);
      if(!userInfos) {
        return res.error('Missing require field(s)', 400);
      }
      // validation
      let allFanpage = await Promise.all(userInfos.map(userInfo => validationAndGetFormat(userInfo)));
      if(allFanpage && allFanpage.length > 0){
        allFanpage = _.flatten(allFanpage);
      }
      return res.success(allFanpage);
    } catch(error){
      res.error(error, 500);
    }
  },
  deleteWorkplace: async (req, res)=>{
    try {
      let workplace = utilities.getValidString(req.params.id);
      if(!workplace){
        return res.error('Missing require field(s)', 400);
      }
      let fanpages = await fanpageSerivce.find({workplace: workplace});
      let fanpageIds = fanpages.map(fp => fp._id);

      let channels = await channelsService.find({ workplace: workplace });
      let channelIds = channels.map(cn => cn._id);
      let data;
      if(fanpageIds && fanpageIds.length !== 0){
        [data] = await Promise.all([
          fanpageSerivce.remove({workplace: workplace}),
          postService.remove({fanpage: {$in: fanpageIds}}),
        ]);
      }
      if(channelIds && channelIds.length !== 0){
        [data] = await Promise.all([
          channelsService.remove({workplace: workplace}),
          youtubeService.remove({channel: { $in: channelIds }}),
        ]);
      }
      await  workplaceService.findByIdAndRemove(workplace);
      return res.success(data, MESSAGE.DELETE_SUCCESS, 200);
    } catch(error) {
      return res.error(error, 500);
    }

  },
  deleteChannelInWorkplace: async  (req, res)=>{
    try {
      let channels = utilities.getValidArray(req.body.channels);
      if(!channels){
        return res.error('Missing require field(s)', 400);
      }
      let youtubeData = await channelsService.find({_id: {$in: channels}});
      await Promise.all([
        channelsService.remove({ _id: {$in: channels }}),
        youtubeService.remove({ channel: {$in: channels }})
      ]);
      return res.success(youtubeData, MESSAGE.DELETE_SUCCESS, 200);
    } catch(error) {
      return res.error(error, 500);
    }
  },
  deleteFanpageInWorkplace: async (req, res)=>{
    try {
      let fanpages = utilities.getValidArray(req.body.fanpages);
      if(!fanpages){
        return res.error('Missing require field(s)', 400);
      }
      let fanpagesData = await fanpageSerivce.find({_id: {$in: fanpages}});
      await Promise.all([
        fanpageSerivce.remove({_id: {$in: fanpages}}),
        postService.remove({fanpage: {$in: fanpages}})
      ]);
      return res.success(fanpagesData, MESSAGE.DELETE_SUCCESS, 200);
    } catch(error) {
      return res.error(error, 500);
    }
  },
  getById: async(req, res)=>{
    try{
      let id = req.params.workplace;
      let workplace = await workplaceService.findById(id);
      if(!workplace){
        return res.error('workplace is not found', 400);
      }
      let fanpages = await fanpageSerivce.find({workplace});
      let data = workplace.toObject();
      data.fanpages = fanpages;
      return res.success(data, 'Get detail success', 200);
    }catch(error) {
      return res.error(error, 500);
    }
  },
  getList: async (req, res)=>{
    try{
      let query = req.query;
      let limit = utilities.getValidNumber(query.limit) || DEFAUL_LIMIT;
      let page = utilities.getValidNumber(query.page) || 1;
      let skip = utilities.getMongoSkip(page, limit);
      let params = {
        name     : utilities.getValidString(query.name),
        type     : utilities.getValidString(query.type),
        status   : utilities.getValidString(query.status),
        createdBy: req.user._id
      };
      if(params.status && STATUS.ENUM.indexOf(params.status) === -1){
        return res.error(`Status is not in ${STATUS.ENUM.toString()}`, 400);
      }
      params = utilities.removeEmptyFields(params);
      params = utilities.buildFullTextSearchObj(params, ['name']);
      let [listWorkplace, total] = await Promise.all([
        workplaceService.find(params, { skip, limit }),
        workplaceService.count(params)
      ]);
      let data = [];
      if(listWorkplace && listWorkplace.length > 0){
        let dataFanpages = await Promise.all(listWorkplace.map(workplace => fanpageSerivce.find({workplace}) ));
        for(let index in listWorkplace){
          let workplace = listWorkplace[index];
          data.push({
            ...workplace.toObject(),
            fanpages: dataFanpages[index]
          });
        }
      }
      let totalPage = utilities.getTotalPage(total, limit);
      return res.success({ page, totalPage, limit, total, data }, MESSAGE.GET_SUCCESS, 200);
    } catch(error) {
      return res.error(error, 500);
    }
  }
};

let validationAndGetFormat = async (userInfo) =>{
  let arrayFormatFanpage = [];
  // validation
  if(!userInfo || !userInfo.user || !userInfo.user._id || !userInfo.groups || !userInfo.workplace || !userInfo.workplace._id){
    throw new APIError('Missing require field', 400, ERROR_TYPE.BAD_REQUEST);
  }
  let [user, workplace] = await Promise.all([
    userService.findOne({_id: userInfo.user._id, status: STATUS.ACTIVE}),
    workplaceService.findOne({ _id: userInfo.workplace._id, status: STATUS.ACTIVE })
  ]);
  if(!user || !workplace){
    throw new APIError(`User is not found with id = ${userInfo.user._id} or workplace is not found with id=${userInfo.workplace._id}`, 400, ERROR_TYPE.BAD_REQUEST);
  }
  let allFanpageAdmin = await facebookHelper.getListFanpage({access_token: user.accessToken}, true);
  for(let fanpage of userInfo.groups){
    if(!fanpage.type){
      throw new APIError('Missing require field', 400, ERROR_TYPE.BAD_REQUEST);
    }
    let isAdmin = false;
    if(fanpage.type === 'fanpage'){
      isAdmin = allFanpageAdmin.find(fp => fp.fanpageId === fanpage.fanpageId) ? true : false;
    }
    // collect fanpage
    let fanpageDb = await fanpageSerivce.findOne({ fanpageId: fanpage.fanpageId, workplace: workplace._id });
    if(!fanpageDb){
      arrayFormatFanpage.push({
        isAdmin        : isAdmin,
        name           : fanpage.name,
        type           : fanpage.type,
        accessToken    : fanpage.accessToken,
        isTokenFBExpire: false,
        picture        : fanpage.picture,
        fanpageId      : fanpage.fanpageId,
        user           : user,
        workplace      : workplace
      });
    }
  }
  // insert
  let fanpages = await Promise.all(arrayFormatFanpage.map(formatFanpage => fanpageSerivce.insertFanpage(formatFanpage)));
  return fanpages;
};
