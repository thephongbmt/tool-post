const accountService = require('./service');
const utilities = require('../../helpers/utilities');
const { DEFAUL_LIMIT } = require('../../config/app');
const { ACCOUNT_ROLE, MESSAGE, STATUS }  = require('../../constant');
module.exports = {
  checkToken: async(req, res)=>{
    // not check when token die because middleware was handle
    return res.success({ alive: true });
  },
  changePassword: async(req, res)=>{
    try{
      let body = req.body;
      let oldpassword = utilities.getValidString(body.oldpassword);
      let password = utilities.getValidString(body.password);
      if(!oldpassword || !password){
        return res.error('Missing old password or password', 400);
      }
      let account = await accountService.findById(req.user._id);
      if(!account){
        return res.error('current account is not found', 400);
      }
      if(!account.comparePassword(oldpassword)){
        return res.error('Old password is not match', 400);
      }
      account.password = password;
      await account.save();
      return res.success(account, 'Change password success', 200);
    }catch(e){
      return res.error(e.message, 500);
    }
  },
  createAccount: async(req, res) => {
    try {
      let body = req.body;
      let data = {
        email   : utilities.getValidString(body.email),
        password: utilities.getValidString(body.password),
        name    : utilities.getValidString(body.name),
        picture : utilities.getValidString(body.password),
        status  : utilities.getValidString(body.status) || STATUS.INACTIVE,
        role    : ACCOUNT_ROLE.CUSTOMER
      };
      if(!data.email || !data.password || !data.status){
        return res.error('Missing require field', 400);
      }
      if(STATUS.ENUM.indexOf(data.status) === -1){
        return res.error(`Status is not in ${STATUS.ENUM.toString()}`, 400);
      }
      let account = await accountService.findOne({ email: data.email });
      if(account){
        return res.error('Email is already exist', 400);
      }
      if (!utilities.isValidEmail(data.email)) {
        return res.error('Email is not valid', 400);
      }
      //let admin = await userService.getAccountAdmin();
      //set token active account
      //data.activeAccountToken = utils.generateRandomString(20, (new Date).getTime());
      let newAccount = await accountService.insertAccount(data);
      // await emailHelper.sendMailTemplate(admin.email, data, 'REGISTER');
      return res.success(newAccount, MESSAGE.GET_SUCCESS, 200);
    } catch (err) {
      res.error(err.message, 500);
    }

  },
  updateAccount: async (req, res) => {
    try {
      let body = req.body;
      let id = req.params.id;
      let params = {
        status  : utilities.getValidString(body.status),
        picture : utilities.getValidString(body.picture),
        password: utilities.getValidString(body.password),
        name    : utilities.getValidString(body.name),
        // role    : utilities.getValidString(body.role)
      };
      // if(!ACCOUNT_ROLE.ENUM.indexOf(params.status) === -1){
      //   return res.error('Role is not valid', 400);
      // }
      if(params.status && STATUS.ENUM.indexOf(params.status) === -1){
        return res.error(`Status is not in ${STATUS.ENUM.toString()}`, 400);
      }
      params = utilities.removeEmptyFields(params);
      let account = await accountService.findById(id);
      if(!account){
        return res.error(`Account is not found with id =${id}`, 400);
      }
      if(params.password){
        account.password = params.password;
      }
      utilities.replaceField(account, params);
      await account.save();
      return res.success(account, MESSAGE.GET_SUCCESS, 200);
    } catch (e){
      return res.error(e.message, 500);
    }
  },
  deleteAccount: async (req, res)=>{
    try {
      let id = req.params.id;
      if(!id){
        return res.error('Missing require id', 400);
      }
      let account = await accountService.findById(id);
      if(!account || account.status === STATUS.DELETE) {
        return res.error(`Account is not found with id =${id}`, 400);
      }
      account.status = STATUS.DELETE;
      await account.save();
      return res.success({ account }, MESSAGE.DELETE_SUCCESS, 200);
    } catch(e) {
      return res.error(MESSAGE.DELETE_FAIL, 500);
    }

  },
  getList: async (req, res)=>{
    try{
      let query = req.query;
      let limit = utilities.getValidNumber(query.limit) || DEFAUL_LIMIT;
      let page = utilities.getValidNumber(query.page) || 1;
      let skip = utilities.getMongoSkip(page, limit);
      let params = {
        name  : utilities.getValidString(query.name),
        email : utilities.getValidString(query.email),
        status: {$ne: STATUS.DELETE}
      };

      params = utilities.removeEmptyFields(params);
      params = utilities.buildFullTextSearchObj(params, ['name', 'email']);
      let [listAccount, total] = await Promise.all([
        accountService.find(params, { skip, limit }),
        accountService.count(params)
      ]);
      let totalPage = utilities.getTotalPage(total, limit);
      return res.success({ page, totalPage, limit, total, listAccount }, MESSAGE.GET_SUCCESS, 200);
    } catch(e) {
      return res.error(`${MESSAGE.GET_FAIL} ${e.message}`, 500);
    }
  }
};