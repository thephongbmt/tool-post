const mongoose = require('mongoose');
const { STATUS } = require('../../constant');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;
// Time Rule Example
// 09:00 <=> 900 , 20:30 <=> 2030
const AccountSchema = new Schema({
  email              : { type: String, unique: true, required: true },
  password           : { type: String, required: true },
  role               : { type: String, enum: ['Admin', 'Customer'], required: true },
  status             : { type: String, enum: STATUS.ENUM, default: STATUS.INACTIVE },
  name               : { type: String },
  phone              : {type: String},
  picture            : { type: String },
  activeAccountToken : { type: String },
  resetPasswordToken : { type: String },
  resetPasswordExpire: { type: Date }
}, {
  timestamps: true, toObject  : {
    transform: function (doc, ret) {
      delete ret.__v;
      delete ret.password;
    }
  },
});

AccountSchema.pre('save', function (next) {
  let account = this;
  if (!account.isModified('password')) {
    return next();
  }

  account.password = account.generatePassword(account.password);
  return next();
});

AccountSchema.methods.generatePassword = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

AccountSchema.methods.comparePassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};


module.exports = mongoose.model('Account', AccountSchema);
