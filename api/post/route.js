
const postController = require('./controller');
module.exports = (route) =>{
  route.get('/post/sync', postController.syncPost);
  route.get('/post', postController.getList);
  route.get('/post/:id', postController.getDetail);

  route.post('/post',  postController.schedulePost);
  route.put('/post/:id/bookmark', postController.updatePost);
  route.delete('/post/:id', postController.deletePost);

  return route;
};
