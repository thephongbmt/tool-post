const postModel = require('./model');
const APIError = require('../../lib/APIError');
const moment = require('moment');
const utility = require('../../helpers/utilities');
const faceBookService = require('../../helpers/facebook');
const fanpageService = require('../fanpage/service');
const { CRON_POST, DATE_FORMAT_SCHEDULE, ERROR_TYPE }  = require('../../constant');
const _ = require('lodash');

module.exports = {
  find: (query, option) =>{
    return postModel.find(query).skip(option.skip).limit(option.limit).sort({postedAt: -1})
      .populate('fanpage', '_id name picture fanpageId accessToken');
  },
  count: (query) =>{
    return postModel.countDocuments(query);
  },
  insertPost: async (post) => {
    return postModel.create(post);
  },
  findById: (id)=>{
    return postModel.findById(id);
  },
  findOne: (param) => {
    return postModel.findOne(param);
  },
  syncPost: async function (fanpages, limit) {
    try{
      let multi = [];
      for(let fanpage of fanpages){
        let posts = await this.syncPostOfFanpage(fanpage, limit);
        for(let post of posts){
          let postDB = await this.findOne({ postId: post.id, fanpage: fanpage._id });
          if(postDB){
            utility.replaceField(postDB, post);
            multi.push(postDB.save());
          } else {
            multi.push(this.insertPost({fanpage: fanpage._id, ...post}));
          }
        }
      }
      return await Promise.all(multi);
    } catch(e) {
      throw new APIError(e, 500, ERROR_TYPE.INTERNAL_SERVER);
    }

  },
  syncPostOfFanpage: async(fanpage, limit)=>{
    try{
      let posts = await faceBookService.getListPost(fanpage.fanpageId, {access_token: fanpage.user.accessToken, limit: limit });
      return posts;
    } catch(e){
      return [];
    }

  },
  remove: async(query)=>{
    try{
      return postModel.remove(query);
    }catch(e){
      throw e;
    }
  },
  create: async(data)=>{
    try{
      return postModel.create(data);
    }catch(e){
      throw e;
    }
  },

  postArticleToFacebook: async (fanpageId, urls = [], content, schedule, token, videoUrl, checkin)=>{
    try {
      const promiseImages = [];
      let allImageIds = [];
      let queryOfFacebook = {};
      let resultImages;
      let urlEndPoint;
      //prepare data
      if (urls && !Array.isArray(urls)) {
        urls = [urls];
      }
      queryOfFacebook.message = content;
      if (schedule) {
        queryOfFacebook.scheduled_publish_time = schedule;
        queryOfFacebook.published = false;
      }
      if(checkin){
        queryOfFacebook.place = checkin;
      }
      if(videoUrl){
        // let videoId = await facebookUploadVideo({
        //   token : token, // with the permission to upload
        //   id    : fanpageId,
        //   stream: stream
        // });
        urlEndPoint = 'videos';
        queryOfFacebook.file_url = videoUrl;
        queryOfFacebook.description = content;
      }else if (!urls || (urls && urls.length !== 1)) {
        urlEndPoint = 'feed';
        for (let url of urls ) {
          promiseImages.push(faceBookService.uploadImagesExpired24h(fanpageId, url, token));
        }
        resultImages = await Promise.all(promiseImages);
        if (resultImages) {
          allImageIds = resultImages.map(o => o.data.id);
          if (allImageIds && allImageIds.length > 1) {
            queryOfFacebook.attached_media = [];
            for (let id of allImageIds) {
              queryOfFacebook.attached_media.push({ media_fbid: id });
            }
          }
        }
      } else if(urls && urls.length === 1){
        urlEndPoint = 'photos';
        queryOfFacebook.url = urls[0];
      }
      let data = await faceBookService.postingToFacebook(urlEndPoint, fanpageId, queryOfFacebook, token);
      return _.get(data, 'data');
    } catch (e) {
      throw e;
    }
  },
  startCronPost: async () => {
    try {
      const fromNow = moment().toDate();
      const toNow = moment().add(12, 'hours').toDate();
      const listPost = await postModel.find({
        schedule      : { $gte: fromNow, $lte: toNow },
        isPostFacebook: false }).populate('fanpage').limit(CRON_POST.LIMIT_POST);
      let total = 0;
      if (listPost) {
        for (let post of listPost) {
          let schedulePost;
          let fanpage = await fanpageService.findOne({ _id: post.fanpage._id});
          if (fanpage) {
            schedulePost = moment(fanpage.schedule, DATE_FORMAT_SCHEDULE).format('X');
            await this.postArticleToFacebook(fanpage.fanpageId, fanpage.urls, fanpage.content, schedulePost, fanpage.token);
            post.schedule = '';
            await post.save();
            total += 1;
          }
        }
      }
      return { total: total };
    } catch (e) {
      throw e;
    }
  },
  syncPostMultiFanpage: async function (limitSync) {
    try {
      let listFanpage = await fanpageService.findWithDistinct('fanpageId', { status: 'active' });
      listFanpage = await fanpageService.find({fanpageId: {$in: listFanpage}});
      if (listFanpage && listFanpage.length) {
        await this.syncPost(listFanpage, limitSync);
      }
      return ;
    } catch (e) {
      throw e;
    }
  }
};
