const mongoose = require('mongoose');
const {STATUS, TYPE_POST, SOCIAL, POST_TO_OBJECT} = require('../../constant/');
let PostSchema = new mongoose.Schema({
  postId        : { type: String, required: true, unique: true },
  from          : { type: String, enum: SOCIAL.ENUM},
  place         : {type: String}, // checkin field value with be fanpageId
  postTo        : { type: String }, // facebookId, fanpageId,userId, .... (thirdparty id)
  postToObject  : { type: String, enum: POST_TO_OBJECT.ENUM},
  type          : { type: String, enum: TYPE_POST.ENUM, default: TYPE_POST.POST },
  status        : { type: String, enum: STATUS.ENUM, default: STATUS.ACTIVE },
  isBookmark    : { type: Boolean, default: false},
  content       : { type: String },
  totalLike     : { type: Number, default: 0},
  totalShare    : { type: Number, default: 0},
  totalComment  : { type: Number, default: 0},
  postedAt      : { type: Date, required: true },
  attachments   : [{ type: String }],
  schedule      : { type: Date },
  isPostFacebook: { type: Boolean, default: false },
  videoUrl      : { type: String },
  isEdited      : { type: Boolean, default: false },
  fanpage       : { type: mongoose.Schema.Types.ObjectId, ref: 'Fanpage' }
}, {
  timestamps: true,
  toObject  : {
    transform: function (doc, ret) {
      delete ret.__v;
    }
  },
});

module.exports = mongoose.model('Post', PostSchema);
