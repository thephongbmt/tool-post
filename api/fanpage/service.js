const fanpageModel = require('./model');
const facebookHelper = require('../../helpers/facebook');


module.exports = {
  getFanpageInfoWithUsers: async (fanpageId, users)=>{
    for(let user of users){
      try{
        if(!user.accessToken){
          continue;
        }
        let fanpageFb = await facebookHelper.getFanpageInfo(fanpageId, {access_token: user.accessToken});
        if(fanpageFb){
          return fanpageFb;
        }
      }catch(e){
        continue;
      }
    }
    return;
  },
  find: (query, option) =>{
    if(!option){
      return fanpageModel.find(query).populate('user', '_id name email facebookId picture accessToken');
    }
    return fanpageModel.find(query).populate('user').skip(option.skip).limit(option.limit);
  },
  count: (query) =>{
    return fanpageModel.countDocuments(query);
  },
  insertFanpage: async (fanpage) => {
    return fanpageModel.create(fanpage);
  },
  findById: (id)=>{
    return fanpageModel.findById(id).populate('user');
  },
  getFanpageByEmail: (email)=>{
    return fanpageModel.findOne({email: email});
  },
  findOne: (param, isPopupate)=>{
    if(isPopupate){
      return fanpageModel.findOne(param).populate('user', '_id facebookId accessToken');
    }
    return fanpageModel.findOne(param);
  },
  findOneAndUpdate: (query, params)=>{
    return fanpageModel.findOneAndUpdate(query, params);
  },
  updateMany: (query, params)=>{
    return fanpageModel.updateMany(query, params);
  },
  remove: (query) =>{
    return fanpageModel.remove(query);
  },
  findOneAndRemove: (query) =>{
    return fanpageModel.findOneAndRemove(query);
  },
  findByIdAndRemove: (id) =>{
    return fanpageModel.findByIdAndRemove(id).populate('user');
  },
  findWithDistinct: (field) => {
    return fanpageModel.distinct(field).populate('user');
  }
};
