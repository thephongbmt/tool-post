
const fanpageController = require('./controller');

module.exports = (route) =>{
  route.get('/facebook/fanpage', fanpageController.getListFanpageByKeyWord);
  route.get('/fanpage', fanpageController.getList);
  route.put('/fanpage/:fanpageId/user/:userId', fanpageController.update);
  route.get('/fanpage/available', fanpageController.getAvailable);
  return route;
};