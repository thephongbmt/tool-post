
const channelController = require('./controller');

module.exports = (route) =>{
  route.get('/channel', channelController.getInformation);
  route.get('/channel/:id/sync', channelController.syncVideoOfChannel);
  route.get('/channel/list', channelController.getListChannel);
  route.post('/channel', channelController.insert);
  route.put('/channel/:id', channelController.updateChannel);
  return route;
};
