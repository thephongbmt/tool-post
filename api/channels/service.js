const channelModel = require('./model');
const youtubeHelper = require('../../helpers/youtube');
const youtubeService = require('../youtube/service');
const utilities = require('../../helpers/utilities');
const _ = require('lodash');

module.exports = {
  find: async (query) => {
    return channelModel.find(query).populate('workplace');
  },
  count: async (query) => {
    return channelModel.countDocuments(query);
  },
  insertChanel: async (data) => {
    return channelModel.create(data);
  },
  findOne: async (query) => {
    return channelModel.findOne(query);
  },
  remove: (query) => {
    return channelModel.remove(query);
  },
  findWithDistinct: (field) => {
    return channelModel.distinct(field);
  },
  getInformationChanel: async (query) =>{
    return await youtubeHelper.getChanel(query);
  },
  syncVideoChannel: async (query) => {
    let params = {
      channelId : utilities.getValidString(query.channelId),
      maxResults: query.limit,
      type      : 'video',
      part      : 'snippet'
    };
    return await youtubeHelper.searchList(params);
  },
  syncVideo: async function (channels, limit) {
    try {
      let multi = [];
      let promise = [];
      for(let channel of channels){
        let dataVideoChannel = await this.syncVideoChannel({channelId: channel.channelId, limit: limit});
        let listVideo = _.get(dataVideoChannel, 'data.items');
        if (listVideo) {
          const listVieoId = listVideo.map(i => i.id.videoId);
          listVieoId.map(id => promise.push(youtubeService.getDetailVideo(id)));
          const result = await Promise.all(promise);
          listVideo = _.flatMap(result, i => i.data.items);
        }
        listVideo = _.map(listVideo, i => utilities.formatValueYoutube(i));
        for(let video of listVideo){
          let videoDB = await youtubeService.findOne({ id: video.id });
          if(videoDB){
            if (!videoDB.channel) {
              videoDB.channel = channel._id;
            }
            utilities.replaceField(videoDB, video);
            multi.push(videoDB.save());
          } else {
            video.channel = channel._id;
            multi.push(youtubeService.insertYoutube(video));
          }
        }
      }
      return await Promise.all(multi);
    } catch (e) {
      throw e;
    }
  },
  syncVideoMultiChannel: async function (limitSync) {
    try {
      let listChannel = await this.findWithDistinct('channelId', { status: 'active' });
      listChannel = await this.find({channelId: {$in: listChannel}});
      if (listChannel && listChannel.length) {
        await this.syncVideo(listChannel, limitSync);
      }
      return ;
    } catch (e) {
      throw e;
    }
  }
};
