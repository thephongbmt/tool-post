const mongoose = require('mongoose');
const chanelService = require('./service');
const workplaceService = require('../workplace/service');
const youtubeService = require('../youtube/service');
const utilities = require('../../helpers/utilities');
const { DEFAUL_LIMIT } = require('../../config/app');
const { MESSAGE } = require('../../constant');
const _ = require('lodash');
const covertMongoObjectId = mongoose.Types.ObjectId;

module.exports = {
  getInformation: async (req, res)=>{
    try{
      let query = req.query;
      let params = {
        id        : utilities.getValidString(query.channelId),
        maxResults: DEFAUL_LIMIT,
        part      : 'snippet, statistics'
      };
      params = utilities.removeEmptyFields(params);
      // option = utilities.removeEmptyFields(option);
      if (!params && !params.id) {
        return res.error('Please enter key in input channelId');
      }
      let result = await chanelService.getInformationChanel(params);
      if (!result) {
        return res.error('No result');
      }
      let chanel = _.get(result, 'data.items[0]');
      let dataChanel = {};
      if (chanel) {
        const channelId = _.get(chanel, 'id');
        const title = _.get(chanel, 'snippet.title');
        const description = _.get(chanel, 'snippet.description');
        const publishedAt = _.get(chanel, 'snippet.publishedAt');
        const urlChanel = _.get(chanel, 'snippet.thumbnails.medium.url');
        const viewCount = _.get(chanel, 'statistics.viewCount');
        const commentCount = _.get(chanel, 'statistics.commentCount');
        const subscriberCount = _.get(chanel, 'statistics.subscriberCount');
        dataChanel = {
          channelId      : channelId,
          title          : title,
          description    : description,
          publishedAt    : publishedAt,
          urlChanel      : urlChanel,
          viewCount      : viewCount,
          commentCount   : commentCount,
          subscriberCount: subscriberCount,
        };
      }
      return res.success(dataChanel, MESSAGE.GET_SUCCESS, 200);
    } catch(e) {
      return res.error(`${MESSAGE.GET_FAIL} ${e.message}`, 500);
    }
  },
  getListChannel: async (req, res) => {
    try {
      let query = req.query;
      let limit = utilities.getValidNumber(query.limit) || DEFAUL_LIMIT;
      let page = utilities.getValidNumber(query.page) || 1;
      let skip = utilities.getMongoSkip(page, limit);
      let workplaceId = query.workplace;
      let title = utilities.getValidString(query.title);
      let params = {};
      if (workplaceId) {
        const workplace = await workplaceService.findOne( { _id: workplaceId });
        if (!workplace ){
          return res.error('workplace doesn\'t exists.');
        }
        params.workplace = workplace._id;
      }
      params.title = title;
      params = utilities.buildFullTextSearchObj(params, ['title']);
      params = utilities.removeEmptyFields(params);
      const [data, total] = await Promise.all([
        chanelService.find(params, { skip, limit }),
        chanelService.count(params)
      ]);
      let totalPage = utilities.getTotalPage(total, limit);
      return res.success({ page, totalPage, limit, total, data }, MESSAGE.GET_SUCCESS, 200);
    } catch (e) {
      return res.error(`${MESSAGE.GET_FAIL} ${e.message}`, 500);
    }
  },
  insert: async (req, res) => {
    try {
      let body = req.body;
      let data = {
        workplace      : utilities.getValidString(body.workplace),
        channelId      : utilities.getValidString(body.channelId),
        title          : utilities.getValidString(body.title),
        description    : utilities.getValidString(body.description),
        viewCount      : utilities.getValidString(body.viewCount),
        commentCount   : utilities.getValidString(body.commentCount),
        publishedAt    : utilities.getValidString(body.publishedAt),
        subscriberCount: utilities.getValidString(body.subscriberCount),
        urlChanel      : utilities.getValidString(body.urlChanel),
      };
      if (!body && !body.workplace) {
        return res.error('missing require filed workplaceId.');
      }
      if (!body && !body.publishedAt){
        return res.error('missing require filed publishedAt.');
      }
      if (!body && !body.channelId){
        return res.error('missing require filed channelId.');
      }
      const channelFound = await chanelService.findOne({ channelId: data.channelId });
      if (channelFound) {
        return res.error('channel exists already.');
      }
      data = utilities.removeEmptyFields(data);
      const workplace = await workplaceService.findOne({ _id: covertMongoObjectId(body.workplace) });
      if (!workplace) {
        return res.error('workplace isn\'t exists.');
      }
      const result = await chanelService.insertChanel(data);
      return res.success(result);
    } catch (e) {
      return res.error(`${MESSAGE.GET_FAIL} ${e.message}`, 500);
    }
  },
  updateChannel: async (req, res) => {
    try {
      const channelId = req.params.id;
      if (!channelId) {
        return res.error('missing require field id.');
      }
      let body = req.body;
      let data = {
        workplace      : utilities.getValidString(body.workplace),
        channelId      : utilities.getValidString(body.channelId),
        title          : utilities.getValidString(body.title),
        description    : utilities.getValidString(body.description),
        viewCount      : utilities.getValidString(body.viewCount),
        commentCount   : utilities.getValidString(body.commentCount),
        publishedAt    : utilities.getValidString(body.publishedAt),
        subscriberCount: utilities.getValidString(body.subscriberCount),
        urlChanel      : utilities.getValidString(body.urlChanel),
      };
      data = utilities.removeEmptyFields(data);
      data.attachments = body.attachments;
      let dataChannel = await chanelService.findOne({ _id: channelId });
      if (!dataChannel) {
        return res.error('Post of Youtube doesn\'t exists.');
      }
      for (let key in data) {
        dataChannel[key] = data[key];
      }
      dataChannel = await dataChannel.save();
      return res.success(dataChannel);
    } catch (e) {
      return res.error(`${MESSAGE.GET_FAIL} ${e.message}`, 500);
    }
  },
  syncVideoOfChannel: async (req, res) => {
    try {
      const channelId = req.params.id;
      const limit = req.query.limit || 10;
      if (!channelId) {
        return res.error('missing require field channelId.');
      }
      const channel = await chanelService.findOne({ _id: channelId });
      if (!channel) {
        return res.error('channel don\'t exists.');
      }
      let params = {
        channelId: channel.channelId,
        limit    : limit
      };
      if(limit > 50 ) {
        params.limit = 50;
      }
      let promise = [];
      const dataVideoChannel = await chanelService.syncVideoChannel(params);
      let listVideo = _.get(dataVideoChannel, 'data.items');

      if (listVideo) {
        const listVieoId = listVideo.map(i => i.id.videoId);
        listVieoId.map(id => promise.push(youtubeService.getDetailVideo(id)));
        const result = await Promise.all(promise);
        listVideo = _.flatMap(result, i => i.data.items);
      }
      listVideo = _.map(listVideo, i => utilities.formatValueYoutube(i));
      let multi = [];
      for(let video of listVideo){
        let videoDB = await youtubeService.findOne({ id: video.id });
        if(videoDB){
          if (!videoDB.channel) {
            videoDB.channel = channel._id;
          }
          utilities.replaceField(videoDB, video);
          multi.push(videoDB.save());
        } else {
          video.channel = channel._id;
          multi.push(youtubeService.insertYoutube(video));
        }
      }
      return res.success(listVideo);
    }catch (e) {
      return res.error(`${MESSAGE.GET_FAIL} ${e.message}`, 500);
    }
  }
};
