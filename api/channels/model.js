const mongoose = require('mongoose');
const { STATUS } = require('../../constant/');
let ChannelSchema = new mongoose.Schema({
  channelId      : {type: String, required: true, unique: true},
  status         : {type: String, enum: STATUS.ENUM, default: STATUS.ACTIVE},
  title          : {type: String},
  description    : {type: String},
  viewCount      : {type: Number, default: 0},
  commentCount   : {type: Number, default: 0},
  subscriberCount: {type: Number, default: 0},
  publishedAt    : {type: Date, required: true},
  urlChanel      : { type: String },
  workplace      : { type: mongoose.Schema.Types.ObjectId, ref: 'Workplace', required: true },
}, {
  timestamps: true,
  toObject  : {
    transform: function (doc, ret) {
      delete ret.__v;
    }
  },
});

module.exports = mongoose.model('Channel', ChannelSchema);
