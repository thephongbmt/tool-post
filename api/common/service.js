const userModel = require('../user/model');
const fanpageModel = require('../fanpage/model');
const youtubeModel = require('../youtube/model');
const postModel = require('../post/model');

const constants = require('../../constant');
let postToConfig = {
  fanpage: {
    postToFieldName: 'fanpageId'
  },
  facebook: {
    postToFieldName: 'facebookId'
  },
  group: {
    postToFieldName: 'fanpageId'
  }
};
module.exports = {
  getPostTo: async (id, postToObject)=>{
    let config = postToConfig[postToObject];
    if(!config){
      return;
    }
    let params = {[config.postToFieldName]: id};
    switch(postToObject){
    case constants.POST_TO_OBJECT.USER:
      return await userModel.findOne(params);
    case constants.POST_TO_OBJECT.GROUP:
      return await fanpageModel.findOne(params);
    case constants.POST_TO_OBJECT.FANPAGE:
      return await fanpageModel.findOne(params);
    default:return;
    }
  },
  getContentResource: async (id, from)=>{
    switch(from){
    case constants.SOCIAL.YOUTUBE:
      return await youtubeModel.findById(id);
    case constants.SOCIAL.FACEBOOK:
      return await postModel.findById(id);
    default:return;
    }
  }
};