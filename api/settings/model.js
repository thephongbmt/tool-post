const mongoose = require('mongoose');
let SettingSchema = new mongoose.Schema({
  enable    : { type: Boolean, required: true, default: false },
  limitSync : { type: Number, default: 10 },
  timeToSync: { type: String, detault: '1D' }, //mininute
  createdBy : { type: mongoose.Schema.Types.ObjectId, ref: 'Account' },
}, {
  timestamps: true,
  toObject  : {
    transform: function (doc, ret) {
      delete ret.__v;
    }
  },
});

module.exports = mongoose.model('Setting', SettingSchema);
