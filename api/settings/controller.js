let settingService = require('./service');
let accountService = require('../account/service');
let cronPostService = require('../post/service');
let cronChannelService = require('../channels/service');
let utility = require('../../helpers/utilities');
let constants = require('../../constant');
let cronService = require('../../helpers/cron');
module.exports = {
  addOrModify: async (req, res)=>{
    try{
      let body = req.body;
      let params = {
        limitSync : utility.getValidNumber(body.limitSync),
        timeToSync: utility.getValidString(body.timeToSync), //minute
        enable    : utility.booleanValidate(body.enable)
      };
      if(req.user.role !== constants.ACCOUNT_ROLE.ADMIN){
        return res.error('You dont have permission to change setting', 403);
      }
      if (!params && !params.timeToSync) {
        return res.error('please chosen time to sync');
      }
      if (!constants.CRON_SETTING.ALL_SETTING_CRON_TIME.includes(params.timeToSync)) {
        return res.error('Time to Sync much be enum=[\'1H\', \'6H\', \'1D\', \'2D\']');
      }
      params = utility.removeEmptyFields(params);
      let admin = await accountService.getAccountAdmin();
      let setting = await settingService.findByAdminSetting(admin._id);
      if(setting){
        utility.replaceField(setting, params);
        setting.save();
      } else{
        params.createdBy = admin._id;
        setting = await settingService.insertSetting(params);
      }
      cronService.manager.update('syncPost', constants.CRON_SETTING[setting.timeToSync], async () => {
        await cronPostService.syncPostMultiFanpage(setting.limitSync);
        await cronChannelService.syncVideoMultiChannel(setting.limitSync);
      });
      return res.success(setting);
    }catch(e){
      return res.error(e);
    }
  }
};
