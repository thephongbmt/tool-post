
let multer  = require('multer');
let maxSize = 2 * 1000 * 1000;
let utility = require('../../helpers/utilities');
let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './assets/public/images');
  },
  filename: function (req, file, cb) {
    let [, typeImage] = file.mimetype.split('/');
    cb(null, `${file.fieldname  }-${  Date.now()}.${typeImage}`);
  }
});

let upload = multer({ storage, limits: maxSize }).array('files', 5);


module.exports = {
  upfile: async(req, res)=>{
    upload(req, res, function (err) {
      if (err instanceof multer.MulterError) {
        return res.error(err.message, 400);
      } else if (err) {
        return res.error(err.message, 500);
      }
      let files = req.files || [];
      let urls = files.map(file => `${utility.getRootUrl(req)}/static/images/${file.filename}`);
      // Everything went fine.
      return res.success(urls, 'Success', 200);
    });
  }
};