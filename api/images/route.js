

const controller = require('./controller');


module.exports = (route) =>{
  route.post('/images/upload', controller.upfile);
  return route;
};