
const userController = require('./controller');

module.exports = (route) =>{
  route.get('/user', userController.getList);
  route.post('/user', userController.createUser);
  route.put('/user/:id', userController.updateUser);
  route.delete('/user/:id', userController.deleteUser);
  return route;
};