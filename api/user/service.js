const userModel = require('./model');
const facebookHelper = require('../../helpers/facebook');

module.exports = {
  getUserActive: async function (accountId){
    let users = await this.find({createdBy: accountId});
    for(let user of users){
      let userFb;
      try {
        userFb = await facebookHelper.checkAliveToken(user.accessToken);
      } catch(e){
        continue;
      }
      if(userFb){
        return user;
      }
    }
    return;
  },
  find: (query, option) => {
    if(option){
      return userModel.find(query).skip(option.skip).limit(option.limit);
    }
    return userModel.find(query, '_id accessToken picture name facebookId');
  },
  count: (query) => {
    return userModel.countDocuments(query);
  },
  insertUser: async (user) => {
    return userModel.create(user);
  },
  findById: (id) => {
    return userModel.findById(id);
  },
  getUserByEmail: (email)=>{
    return userModel.findOne({ email: email });
  },
  findOne: (param) => {
    return userModel.findOne(param);
  },
  findOneAndUpdate: (query, params) => {
    return userModel.findOneAndUpdate(query, params);
  }
};
