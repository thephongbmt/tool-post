const mongoose = require('mongoose');
const { STATUS } = require('../../constant');
const Schema = mongoose.Schema;
// Time Rule Example
// 09:00 <=> 900 , 20:30 <=> 2030
const UserSchema = new Schema({
  facebookId     : {type: String, required: true},
  status         : { type: String, enum: STATUS.ENUM, default: STATUS.ACTIVE },
  accessToken    : { type: String, required: true },
  name           : { type: String },
  isTokenFBExpire: { type: Boolean, default: true},
  email          : { type: String, unique: true },
  picture        : { type: String },
  createdBy      : { type: mongoose.Schema.Types.ObjectId, ref: 'Account', required: true },
}, {
  timestamps: true, toObject  : {
    transform: function (doc, ret) {
      delete ret.__v;
      delete ret.password;
    }
  },
});

module.exports = mongoose.model('User', UserSchema);
