const httpStatus = require('http-status');

module.exports = class APIError extends Error {
  constructor(message, status = httpStatus.INTERNAL_SERVER_ERROR, type) {
    super(message);
    this.message = message;
    this.status = status;
    this.type = type;
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor.name);
  }
  toString() {
    return `${this.type} - ${this.name} - ${this.message}`;
  }
};
