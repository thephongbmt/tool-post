const moment = require('moment');
const _ = require('lodash');
module.exports = {
  getTotalPage: (total = 0, limit = 10) => {
    return Math.ceil(total / limit);
  },
  getRandomNumber: (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },
  getRootUrl: (req) => {
    let hostName = req.get('host');
    return `http://${hostName}`;
  },
  getFullUrl: (req) => {
    return module.exports.getRootUrl(req) + req.originalUrl;
  },

  isValidEmail: (email) => {
    let emailRegEx = /^[-a-z0-9~!$%^&*_=+}{'?]+(\.[-a-z0-9~!$%^&*_=+}{'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
    return emailRegEx.test(email);
  },

  generateRandomString: (number, prefix) => {
    (typeof (number) === 'number' && number > 0) ? number : false;
    if (!number) return false;
    let possibleCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let str = '';
    for (let i = 0; i < number; i++) {
      let randomCharacter = possibleCharacters.charAt(Math.floor(Math.random() * possibleCharacters.length));
      str += randomCharacter;
    }
    return prefix + str;
  },

  removeSpecialCharacters: (str) => {
    str = str.toLowerCase();
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\-|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|$|_|“|”|‘|’|…|–/g, '');
    /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
    // str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
    // str = str.replace(/^\-+|\-+$/g, "");
    // cắt bỏ ký tự - ở đầu và cuối chuỗi
    return str;
  },
  removeUnicode: (str) => {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    str = str.replace(/đ/g, 'd');

    let strArr = str.split('');
    let safeArr = [];
    for (let x = 0; x < strArr.length; ++x) {
      if (['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '-', '_', '.'].indexOf(strArr[x]) >= 0) {
        safeArr.push(strArr[x]);
      }
    }
    return safeArr.join('');
  },

  titleToUrlString: (str) => {
    str = module.exports.removeUnicode(module.exports.removeSpecialCharacters(str)).trim().replace(/\ /g, '-');
    let x = false;
    for (let i = 0; i < str.length; i++) {
      if (str[i] == '-' && x == true) {
        str = str.replace(/\--/g, '-');
        i--;
      } else if (str[i] == '-') {
        x = true;
      } else {
        x = false;
      }
    }
    return str;
  },
  buildUrl: (query) => {
    let result = [];
    let builtQueryURL = (objURL, parentDirect = []) => {
      if (Array.isArray(objURL)) {
        for (let i = 0; i < objURL.length; i++) {
          let value = objURL[i];
          builtQueryURL(value, [...parentDirect, i]);
        }
      } else if (typeof objURL === 'object') {
        for (let key in objURL) {
          let value = objURL[key];
          builtQueryURL(value, [...parentDirect, key]);
        }
      } else {
        if (objURL !== undefined && objURL !== null && objURL !== '') {
        // encode value in url
          let parentKey = parentDirect[0];
          let child = parentDirect.slice(1, parentDirect.length + 1);
          let fullKey = child.length ? `${parentKey}[${child.join('][')}]` : parentKey;
          result.push(`${fullKey}=${encodeURIComponent(objURL)}`);
        }
      }
    };
    builtQueryURL(query);
    return result.join('&');
  },

  getMongoSkip: (page, limit) => {
    return (page - 1) * limit;
  },
  getValidNumber: (number) => {
    return isNaN(number) && [undefined, null, ''].indexOf(number) > -1 ? undefined : +number;
  },
  getValidString: (str) => {
    return typeof str === 'string' && str && str.trim() ? str.trim() : undefined;
  },
  getValidDate: (date) => {
    return typeof date === 'string' && moment(date).isValid() ? date : undefined;
  },
  getValidItemInArr: (str, arr) => {
    return arr.indexOf(str) !== -1;
  },
  getValidArray: (array)=>{
    return Array.isArray(array) ? array : undefined;
  },
  buildFullTextSearchObj: function (params, fieldsToFullTextSearch = []) {
    let tmp = { ...params};
    if (fieldsToFullTextSearch.length === 0) {
      return tmp;
    }
    fieldsToFullTextSearch.forEach((field) => {
      let value = tmp[field];
      if (value) {
        tmp[field] = { $regex: new RegExp(this.fullTextSearch(value), 'i')};
      }
    });
    return tmp;
  },
  buildSearchInDate: function (params, dateFields = []) {
    let tmp = { ...params};
    if (dateFields.length === 0) {
      return tmp;
    }
    dateFields.forEach((field) => {
      let value = tmp[field];
      if (value && moment(value).isValid) {
        let currentDate = moment(value).format('YYYY-MM-DD');
        let startDate = `${currentDate} 00:00:00`;
        let endDate = `${currentDate} 23:59:59`;
        tmp[field] = { $gte: startDate, $lt: endDate};
      }
    });
    return tmp;
  },
  fullTextSearch: (str) => {
    if (typeof (str) !== 'string') {
      return str;
    }
    str = str.replace(/  +/g, ' ');
    str = str.replace(/a/gi, '[AÁÀẢÃẠÂẤẦẨẪẬĂẮẰẲẴẶ]');
    str = str.replace(/e/gi, '[EÉÈẺẼẸÊẾỀỂỄỆ]');
    str = str.replace(/i/gi, '[IÍÌỈĨỊ]');
    str = str.replace(/o/gi, '[OÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢ]');
    str = str.replace(/u/gi, '[UÚÙỦŨỤƯỨỪỬỮỰ]');
    str = str.replace(/y/gi, '[YÝỲỶỸỴ]');
    str = str.replace(/d/gi, '[DĐ]');
    return str;
  },
  getFileName: (fileName) => {
    if (!fileName) {
      return [];
    }
    let path = fileName.split('.');
    path = path.slice(0, path.length - 1).join('');
    return path;
  },
  getCurrentDate: (format) => {
    let array = format.split('/');
    let current = new Date();
    // month +1 because in new Date of javascript, month start at zero
    let objDate = {
      YYYY: current.getFullYear(),
      MM  : current.getMonth() + 1,
      DD  : current.getDate(),
    };
    // handle format date
    let stringFormat = [];
    for (let i of array) {
      stringFormat.push(objDate[i]);
    }
    return stringFormat.join('/');
  },
  generateFileNameDestination: function (file) {
    let fileName = this.getFileName(file.originalname);
    let covertName = this.titleToUrlString(fileName);
    let destFileName = `${this.getCurrentDate('YYYY/MM/DD')}/${covertName}-${(new Date()).getTime()}.${file.extension}`;
    return destFileName;
  },
  removeEmptyFields: (obj) => {
    return _.pickBy(obj, (e)=> e !== undefined && e !== undefined);
  },
  replaceField: (obj, newObj) => {
    for(let key in newObj){
      if(newObj[key] !== undefined){
        obj[key] = newObj[key];
      }
    }
    return obj;
  },
  getValidDateUnixPostSchedule: (date, format) => {
    return typeof date === 'string' && moment(date, format).isValid() ? moment(date, format).format('X') : undefined;
  },
  getValidDateWithSubUnixPostSchedule: (date, format, number, type) => {
    return typeof date === 'string' && moment(date, format).isValid() ? moment(date, format).subtract(number, type).format('X') : undefined;
  },
  booleanValidate: (flag) => {
    if (flag === 'true' || flag === true) {
      return true;
    }
    if (flag === 'false' || flag === false) {
      return false;
    }
    return;
  },
  formatValueYoutube: (obj) => {
    if (obj && obj.snippet){
      for (let key in obj.snippet) {
        obj[key] = obj.snippet[key];
      }
      delete obj.snippet;
    }
    if (obj && obj.statistics) {
      for (let key in obj.statistics) {
        obj[key] = obj.statistics[key];
      }
      delete obj.statistics;
    }
    if (obj && obj.thumbnails && obj.thumbnails.standard) {
      obj['attachments'] = [obj.thumbnails.standard.url];
      delete obj.thumbnails;
    }
    obj.link = `https://www.youtube.com/watch?v=${obj.id}`;
    delete obj.localized;
    delete obj.kind;
    delete obj.etag;
    delete obj.tags;
    delete obj.categoryId;
    delete obj.liveBroadcastContent;
    return obj;
  },
  replaceValueQueryInUrl: (url, fieldName, value) =>{
    if(!url || !fieldName || !value){
      return url;
    }
    // remove old field in url and add new field into url
    let [domain = [], paramsStr = []] = url.split('?');
    // find old field in url
    let paramsArray = paramsStr.split('&');
    let indexAccessToken = paramsArray.findIndex(param => param.indexOf(fieldName) > -1);
    if(indexAccessToken === -1){
      return url;
    }
    //remove old field
    paramsArray.splice(indexAccessToken, 1);
    // add new field
    paramsArray.push([fieldName, value].join('='));
    // rebuild url
    paramsStr =  paramsArray.join('&');
    url = [domain, paramsStr].join('?');
    return url;
  },
  checkArrayInArray: (rootArray, targetArray)=>{
    for(let root of rootArray){
      let exist = targetArray.find(target => target === root );
      if(!exist){
        return false;
      }
    }
    return true;
  }
};
