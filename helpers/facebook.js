const _ = require('lodash');
const axios = require('axios');
const Log = require('../lib/Log.class');
const ultility = require('../helpers/utilities');
const APIError = require('../lib/APIError');
const { FACEBOOK } = require('../config/app');
const { ERROR_TYPE, TYPE_FANPAGE } = require('../constant/index');

let buildFacebookUrl = (route, query) => {
  let url = `${FACEBOOK.URL}/${route}?${ultility.buildUrl(query, 'snake')}`;
  Log.info(`Request ${url}`);
  return url;
};
let getMessageError = (error) =>{
  let messageRespone = _.get(error, 'response.data.error.message') || error.message;
  let status = _.get(error, 'response.status') || error.status;

  if(messageRespone){
    return { message: messageRespone, status};
  }
  return { message: messageRespone.message, status };
};
module.exports = {
  getListFanpageByKeyWord: async(keywork, accessToken)=>{
    try {
      let params = {
        q           : keywork,
        limit       : 5,
        fields      : 'id,name,picture',
        access_token: accessToken
      };
      let response = await axios({
        method: 'get',
        url   : `${buildFacebookUrl('/pages/search', params)}`,
        data  : params
      });
      let data = response.data;
      data.picture = _.get(data, 'picture.data.url');
      return data;
    } catch(e) {
      let { message = 'Error', status } = getMessageError(e);
      throw new APIError(message, status, ERROR_TYPE.CALLING_FACEBOOK_FAIL);
    }
  },
  getProfile: async (params) => {
    try {
      let response = await axios({
        method: 'get',
        url   : `${buildFacebookUrl('me', params)}`,
        data  : params
      });
      let data = response.data;
      data.picture = _.get(data, 'picture.data.url');
      return data;
    } catch(e) {
      let { message = 'Error', status } = getMessageError(e);
      throw new APIError(message, status, ERROR_TYPE.CALLING_FACEBOOK_FAIL);
    }
  },
  getDetailFanpage: async (fanpageid, query) => {
    try {
      query.fields = query.fields || 'picture,name,id,fan_count,access_token';
      let response  = await axios({
        method: 'get',
        url   : `${buildFacebookUrl(fanpageid, query)}`,
      });
      let fanpage = response.data;
      fanpage = {
        fanpageId  : fanpage.id,
        accessToken: fanpage.access_token,
        name       : fanpage.name,
        picture    : _.get(fanpage, 'picture.data.url'),
      };
      return fanpage;
    } catch(e) {
      let { message, status } = getMessageError(e);
      throw new APIError(message, status, ERROR_TYPE.CALLING_FACEBOOK_FAIL);
    }
  },
  checkFanpageTokenAlive: async function(fanpageId, accessToken){
    try{
      await this.getListPost(fanpageId, {
        access_token: accessToken,
        fields      : 'id,limit(1)'
      });
      return true;
    }catch(e){
      return false;
    }
  },
  getGroup: async(facebookUserId, query) => {
    try {
      query.fields = query.fields || 'picture,id,name';
      let data = await axios({
        method: 'get',
        url   : buildFacebookUrl(`${facebookUserId}/groups`, query)
      });
      if(!data.data || data.data.length === 0){
        return [];
      }
      let fanpages  = data.data.data.map(fanpage => {
        return {
          fanpageId: fanpage.id,
          type     : TYPE_FANPAGE.GROUP,
          name     : fanpage.name,
          picture  : _.get(fanpage, 'picture.data.url'),
        };
      });
      return fanpages;
    } catch (e) {
      let { message, status } = getMessageError(e);
      throw new APIError(message, status, ERROR_TYPE.CALLING_FACEBOOK_FAIL);
    }
  },
  getFanpageInfo: async(fanpageId, query) =>{
    try {
      query.fields = query.fields || 'picture,id,name,fan_count,access_token,__type__';
      let response;
      try{
        response = await axios({
          method: 'get',
          url   : `${buildFacebookUrl(`/${fanpageId}`, query)}`,
        });
      } catch(e) {
        Log.error(e);
        query.fields = 'picture,id,name,__type__';
        try{
          response = await axios({
            method: 'get',
            url   : `${buildFacebookUrl(`/${fanpageId}`, query)}`,
          });
        }catch(e){Log.error(e);
          query.fields = 'picture,id,name';
          try{
            response = await axios({
              method: 'get',
              url   : `${buildFacebookUrl(`/${fanpageId}`, query)}`,
            });
          }catch(e){
            Log.error(e);
            throw e;
          }
        }
      }
      let data = response.data;
      if(!data){
        return;
      }
      let type = _.get(data, '__type__.name', 'fanpage');
      switch(type){
      case 'Group': type = TYPE_FANPAGE.GROUP; break;
      case 'Page': type = TYPE_FANPAGE.FANPAGE; break;
      default: type = 'fanpage';
      }
      return {
        fanpageId  : data.id,
        accessToken: data.access_token || query.access_token,
        type       : type,
        name       : data.name,
        picture    : _.get(data, 'picture.data.url')
      };
    } catch(e) {
      Log.error(e);
      throw e;
    }
  },
  getListFanpage: async (query, isAdmin) => {
    try {
      query.fields = query.fields || 'picture,id,name,fan_count,access_token,tasks';
      let response = await axios({
        method: 'get',
        url   : `${buildFacebookUrl('/me/accounts', query)}`,
      });
      let data = response.data;
      if(!data.data || data.data.length === 0){
        return [];
      }
      let fanpages  = data.data.map(fanpage => {
        if(isAdmin){
          if(!ultility.checkArrayInArray(['ADVERTISE', 'ANALYZE', 'CREATE_CONTENT', 'MODERATE'], fanpage.tasks)){
            return;
          }
          return {
            fanpageId  : fanpage.id,
            accessToken: fanpage.access_token,
            type       : TYPE_FANPAGE.FANPAGE,
            name       : fanpage.name,
            picture    : _.get(fanpage, 'picture.data.url'),
          };
        }
        return {
          fanpageId  : fanpage.id,
          accessToken: fanpage.access_token,
          type       : TYPE_FANPAGE.FANPAGE,
          name       : fanpage.name,
          picture    : _.get(fanpage, 'picture.data.url'),
        };
      });

      return fanpages || [];
    } catch(e){
      let { message, status } = getMessageError(e);
      throw new APIError(message, status, ERROR_TYPE.CALLING_FACEBOOK_FAIL);
    }
  },
  getQualityVideo: async(id, query)=>{
    try {
      query.fields = query.fields || 'source';
      let response = await axios({
        method: 'get',
        url   : `${buildFacebookUrl(`/${id}`, query)}`,
      });
      let data = response.data;
      return data;
    } catch(e) {
      throw e;
    }
  },
  getListPost: async function(fanpageId, query)  {
    try {
      let getQualityVideoFnc = this.getQualityVideo;
      query.fields = query.fields || 'message,story,attachments,created_time,picture,id,shares,likes.summary(1),comments.summary(1)';
      let response  = await axios({
        method: 'get',
        url   : `${buildFacebookUrl(`${fanpageId}/feed`, query)}`,
      });
      let posts = response.data.data;
      if(!posts || posts.length === 0){
        return [];
      }
      posts = await Promise.all(posts.map(async post => {
        if(!post){
          return;
        }
        let attachments = _.get(post, 'attachments.data', []);
        let videoUrl = _.get(post, 'attachments.data.0.media.source', '');
        let videoId = _.get(post, 'attachments.data.0.target.id', '');
        if(videoUrl && videoId){
          let videoFb = await getQualityVideoFnc(videoId, {access_token: query.access_token});
          if(videoFb && videoFb.source){
            videoUrl = videoFb.source;
          }
        }
        let subattachments =  _.get(post, 'attachments.data.0.subattachments.data', []);
        attachments = attachments.concat(subattachments);
        let tmpPost = {
          id          : post.id,
          postedAt    : post.created_time,
          content     : post.message || post.story,
          postId      : post.id,
          from        : 'facebook',
          videoUrl    : videoUrl,
          attachments : attachments.map(attachment => _.get(attachment, 'media.image.src')).filter(e=>e),
          createdTime : post.created_time,
          picture     : post.picture,
          totalLike   : _.get(post, 'likes.summary.total_count', 0),
          totalShare  : _.get(post, 'shares.count', 0),
          totalComment: _.get(post, 'comments.summary.total_count', 0)
        };
        return tmpPost;
      }));
      posts = posts.filter(e=>e);
      return posts;
    } catch(e) {
      let { message, status } = getMessageError(e);
      throw new APIError(message, status, ERROR_TYPE.CALLING_FACEBOOK_FAIL);
    }
  },
  uploadImagesExpired24h: async (fanpageId, url, token) => {
    try {
      let data = await axios({
        method: 'post',
        url   : `${FACEBOOK.URL}/${fanpageId}/photos?access_token=${token}`,
        data  : {
          'url'      : url,
          'published': false,
        }
      });
      return data;
    } catch (e) {
      throw new APIError(e.message, 500, ERROR_TYPE.CALLING_FACEBOOK_FAIL);
    }
  },
  // getDataWithFanpageToken: async(fanpageId, url, option) => {
  //   try{
  //     let isExpire = false;
  //     if(!fanpageId){
  //       throw new APIError('Missing fanpageId', 500, ERROR_TYPE.INTERNAL_SERVER);
  //     }
  //     try {
  //       let data = await axios({
  //         method: option.method,
  //         url   : url,
  //         data  : option.data
  //       });
  //       return data;
  //     } catch(err){
  //       let error = err.response.data.error;
  //       if (error.type === 'OAuthException' && error.code === 190) {
  //         isExpire = true;
  //         Log.warn('Token user is expired');
  //       }
  //     }
  //     if(!isExpire){
  //       throw new APIError('Token user is expired', 500, ERROR_TYPE.CALLING_FACEBOOK_FAIL);
  //     }
  //     // remove old tolen in url and add new token into url
  //     let newUrl = ultility.replaceValueQueryInUrl(url, 'access_token', fanpageDB.accessToken);
  //     // recall api
  //     let data = await axios({
  //       method: option.method,
  //       url   : newUrl,
  //       data  : option.data
  //     });
  //     return data;
  //   }catch(e){
  //     let { message, status } = getMessageError(e);
  //     throw new APIError(message, status, ERROR_TYPE.CALLING_FACEBOOK_FAIL);
  //   }
  // },
  postingToFacebook: async function (urlEndPoint, fanpageId, data, token)  {
    try {
      let result = await axios({
        url   : `${FACEBOOK.URL}/${fanpageId}/${urlEndPoint}?access_token=${token}`,
        method: 'post',
        data  : data
      });
      return result;
    } catch (e) {
      Log.error(e);
      let { message, status } = getMessageError(e);
      throw new APIError(message, status, ERROR_TYPE.CALLING_FACEBOOK_FAIL);
    }
  },
  checkAliveToken: async (token) => {
    try {
      let response = await axios({
        method: 'get',
        url   : `${buildFacebookUrl('me', {access_token: token})}`,
      });
      let data = response.data;
      data.picture = _.get(data, 'id');
      return true;
    } catch(e) {
      return false;
    }
  }
};
