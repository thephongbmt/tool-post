//const Raven = require( 'raven';
const { MESSAGE } = require('../constant');
const httpStatus = require('http-status');
const APIError = require('../lib/APIError');
const middleware = {
  /*
      ERR: handle for error response,
      success: handle for  success response
    */
  handleResponse: app =>
    app.use((req, res, next) => {
      let resSuccess = (data = undefined, message = 'Request Success', code = httpStatus.OK) => {
        return res.status(code).json({ status: true, message, data });
      };

      let resError = (message = MESSAGE.ERROR_MESSAGE_DEFAULT, code = httpStatus.INTERNAL_SERVER_ERROR) => {
        if(typeof  message === 'object'){
          message = message.message;
        }
        return res.status(code).json({status: false, message: message});
      };
      // add in request
      res.success = resSuccess;
      res.error = resError;
      next();
    }),
  /*
    Handle for unknown URL request to ser ver
  */
  handleNotFoundRequest: app => {
    app.use((req, res, next) => {
      return next(res.error(MESSAGE.NOT_FOUND_ROUTE, httpStatus.NOT_FOUND, false));
    });
  },
  /*
    Handle express  error = require( route express
  */
  handleError: app =>
    app.use((err, req, res, next) => {
      let apiError = _convertErr(err.message, err.status);
      let message =
        req.query.error === '1'
          ? {
            status : false,
            message: apiError.message,
            stack  : apiError.stack
          }
          : {
            status : false,
            message: apiError.message //${apiError.status}-${httpStatus[`${apiError.status}_NAME`]}`
          };

      res.status(apiError.status).json(message);
      //return Raven.captureException(err);
      return next();
    })
};

/***
 * PRIVATE FUNCTION
 */

/*
  covert error to custom API Error
*/
const _convertErr = (error, status) => {
  let message;
  if (typeof error === 'string') {
    //for string error message
    message = error;
  } else if (error instanceof Array) {
    //for array object error message
    message = error.map(err => err.message);
  }  else if (typeof error === 'object') {
    //for other message
    message = error.message ? error.message : error;
  }

  return new APIError(message, status);
};

module.exports = {
  middleware
};